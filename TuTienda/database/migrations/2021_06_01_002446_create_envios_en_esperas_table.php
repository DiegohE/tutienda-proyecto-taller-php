<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnviosEnEsperasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envios_en_esperas', function (Blueprint $table) {
            $table->string('codigoEnvio', 100)->primary();
            $table->timestamps();

            $table->foreign('codigoEnvio')->references('codigoEnvio')->on('pedidos')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envios_en_esperas');
    }
}
